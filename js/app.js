var app = angular.module('mainApp', ['drag', 'ui.router', 'ngAnimate']);

app.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/home');
        $stateProvider
            .state('home', {
                url: '/home',
                views: {
                    '': {templateUrl: 'partials/partial-home.html'},
                    'viewA@home': {templateUrl: 'partials/partial-home-scheme.html'},
                    'viewB@home': {
                        templateUrl: 'partials/partial-home-list.html'}
                }
            }
        ).state('map', {
                url: '/map',
                templateUrl: 'partials/partial-map.html'
        }).state('user', {
                url: '/user/:id',
                templateUrl: 'partials/partial-home-userinfo.html',
                controller: function($scope, $stateParams, $state){
                    if($scope.users.hasOwnProperty($stateParams.id)){
                    $scope.user = $scope.users[$stateParams.id];}
                    else{
                        $state.go('home');
                    }
                }
            });
    }
);