/**
 * Created by roman on 6/18/14.
 */
var router = angular.module('router', []).config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('route1',{
            templateUrl: "partials/partial-home-scheme.html"
        })
        .state('route2',{
            templateUrl: "partials/partial-home-list.html"
        }

    )
});