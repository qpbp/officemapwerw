/**
 * Created by Roma on 21.05.14.
 */

var drag = angular.module('drag', []).directive('draggable', function ($document, $http) {
    return function (scope, element, attr, http) {

        var userInfo = function (scope) {
            return '<img style="border-radius: 50%; width: 75px; height: 75px;" src="' + scope.users[attr.index].photo + '">' +
                '<div>Post: ' + scope.users[attr.index].post + '</div>' +
                '<div>Age:' + scope.users[attr.index].age + '</div>';
        }

        var user_x = scope.users[attr.index].position.x,
            user_y = scope.users[attr.index].position.y;

        var startX = user_x,
            startY = user_y,
            x = user_x,
            y = user_y;

        element.css({
            position: 'relative',
            //border: '1px solid red',
            //backgroundColor: 'lightgrey',
            cursor: 'pointer',
            top: user_y + 'px',
            left: user_x + 'px'
        });
        //element popover
        element.popover({
            trigger: 'hover',
            title: scope.users[attr.index].first_name + ' ' + scope.users[attr.index].last_name,
            //directive? or else
            content: userInfo(scope),
            placement: 'top',
            html: true
        });
        /*element.on('dblclick', function(event){});*/
        element.bind('mousedown', function (event) {
            // Prevent default dragging of selected content
            event.preventDefault();
            startX = event.screenX - x;
            startY = event.screenY - y;
            $document.on('mousemove', mousemove);
            $document.on('mouseup', mouseup);

        }); //was 'on'

        function mousemove(event) {
            y = event.screenY - startY;
            x = event.screenX - startX;
            element.css({
                top: y + 'px',
                left: x + 'px'
            });
        }

        function mouseup(event) {
            $document.off('mousemove', mousemove);
            $document.off('mouseup', mouseup);
            //scope.users[attr.index].position.x = event.screenX;
            //scope.users[attr.index].position.y = event.screenY;
            console.log(event.screenX + ' ' + event.screenY);
        }


    };
});

