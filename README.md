OfficeMap
=========
The project for W. Soft.

1. npm install
2. bower install
3. Import the test admin acccount data to 'test' collection in your mongodb:


```
#!javascript

{
        "provider" : "local",
        "email" : "mod@mod.ua",
        "username" : "moder",
        "hashedPassword" : "0n3iVq8zQSRZF1quo2wGd3RMDuGGOQFZuGQ6sqyt42ehgCD/tdW9ZdsEvzowV7l4XbINCK5P+W5nT1ht2gOT2Q==",
        "salt" : "il22b16yS7+KTgLG03m33g==",
        "usertype" : "admin",
        "__v" : 0
}
```
or run in your mongodb 'users' collection:


```
#!javascript

db.users.insert({"provider":"local","email":"mod@mod.ua","username":"moder","hashedPassword":"0n3iVq8zQSRZF1quo2wGd3RMDuGGOQFZuGQ6sqyt42ehgCD/tdW9ZdsEvzowV7l4XbINCK5P+W5nT1ht2gOT2Q==","salt":"il22b16yS7+KTgLG03m33g==","usertype":"admin","__v":0})
```

4. nodemon server/index.js

5. Run Selenium:


```
#!javascript

./node_modules/protractor/bin/webdriver-manager start
```
6. Run tests:

protractor test/conf.js